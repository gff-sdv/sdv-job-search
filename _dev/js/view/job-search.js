const JobSearch = (function () {
  /**
   * Escapes html entities.
   *
   * @param {string} str
   * @returns {string}
   *
   * @private
   */
  const _escapeHTML = (str) => {
    const p = document.createElement('p');
    p.appendChild(document.createTextNode(str));
    return p.innerHTML;
  };

  /**
   * Initializes all job searches on the current document.
   *
   * @private
   */
  const _init = () => {
    [].forEach.call(document.querySelectorAll('.job-search'), _initJobSearch);
  };

  /**
   * Initializes a given job search.
   *
   * @param {Element} element
   * @private
   */
  const _initJobSearch = element => {
    const detailsLabel = 'Hier gehts zu den Details';
    const apprenticeshipsLabel = 'Lehrstellen';
    const mode = element.getAttribute('data-job-search-mode');

    StoreLocator.LocationSearch.init(
      element,
      function (element, lat, lng) {
        const search_url = element.getAttribute('data-search-url');
        if (search_url === null) {
          // The search url has to be defined as data attribute.
          return null;
        }

        return search_url + '&lat=' + lat + '&lng=' + lng;
      },
      function (result) {
        if (mode === 'trial_apprenticeship') {
          return '<div class="job-search-result">' + _escapeHTML(result.company_name) + '<br>' +
            _escapeHTML(result.zip) + ' ' + _escapeHTML(result.city) + '<br>' +
            '<div class="wp-block-buttons"><div class="wp-block-button is-style-simple"><a class="wp-block-button__link" href="' + result.profile_link + '" target="_blank" rel="noopener">' + detailsLabel + '</a></div></div>' +
            '</div>';
        }

        return '<div class="job-search-result"><div>' + _escapeHTML(result.company_name) + '<br>' +
          _escapeHTML(result.zip) + ' ' + _escapeHTML(result.city) + '</div>' +
          '<div class="job-search-result-apprenticeships">' + apprenticeshipsLabel + '<br>' +
          '<span class="' + (result.year_1[1] === true ? 'available' : 'not-available') + '">' + _escapeHTML(result.year_1[0]) + '</span> | ' +
          '<span class="' + (result.year_2[1] === true ? 'available' : 'not-available') + '">' + _escapeHTML(result.year_2[0]) + '</span> | ' +
          '<span class="' + (result.year_3[1] === true ? 'available' : 'not-available') + '">' + _escapeHTML(result.year_3[0]) + '</span></div>' +
          '<div class="wp-block-buttons"><div class="wp-block-button is-style-simple"><a class="wp-block-button__link" href="' + result.profile_link + '" target="_blank" rel="noopener">' + detailsLabel + '</a></div></div>' +
          '</div>';
      });
  };

  return {
    init: _init
  };
})();