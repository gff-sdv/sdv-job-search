

if (document.readyState !== 'loading') {
  JobSearch.init();
} else {
  document.addEventListener('DOMContentLoaded', JobSearch.init);
}
