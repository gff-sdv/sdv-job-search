module.exports = function (grunt) {
  require('load-grunt-tasks')(grunt);

  let jsView = [
    'js/view/job-search.js',
    'js/view/view.js'
  ];

  let cssView = [
    'tmp/view.css'
  ];

  // noinspection JSUnresolvedFunction
  grunt.initConfig({
    makepot: {
      main: {
        options: {
          cwd: '../',
          mainFile: 'sdv-job-search.php',
          domainPath: 'locales',
          potHeaders: {
            'Report-Msgid-Bugs-To': 'adrian.suter@gff.ch'
          },
          exclude: ['_dev'],
          processPot: function (pot) {
            delete pot['translations']['']['https://www.gff.ch'];
            delete pot['translations']['']['GFF Integrative Kommunikation GmbH'];
            delete pot['translations']['']['Settings'];

            return pot;
          }
        }
      }
    }
    ,
    uglify: {
      options: {
        mangle: true,
        compress: true
      },
      jsView: {files: {'../scripts/view.js': jsView}}
    }
    ,
    sass: {
      options: {
        trace: false,
        sourcemap: 'none',
        style: 'compressed'
      },
      cssView: {
        files: [{expand: true, cwd: 'scss/view', src: 'view.scss', dest: 'tmp', ext: '.css'}]
      }
    }
    ,
    cssmin: {
      options: {
        mergeIntoShorthands: false,
        roundingPrecision: -1
      },
      cssView: {files: {'tmp/view.complete.css': cssView}}
    }
    ,
    postcss: {
      options: {
        processors: [
          require('autoprefixer')
        ]
      },
      cssView: {files: {'../styles/view.css': 'tmp/view.complete.css'}}
    }
    ,
    watch: {
      options: {spawn: false},
      jsView: {files: jsView, tasks: ['uglify:jsView']},
      sassAll: {
        files: ['scss/**/*.scss'],
        tasks: ['sass', 'cssmin', 'postcss']
      }
    }
    ,
    compress: {
      main: {
        options: {
          archive: function () {
            return '../sdv-job-search.zip';
          },
          mode: 'zip'
        },
        files: [
          {
            expand: true,
            cwd: '../',
            src: [
              '**',
              '!_dev/**',
              '!assets/*.xcf',
              '!.gitignore',
              '!.gitlab-ci.yml',
              '!README.md',
            ],
            dest: 'sdv-job-search/'
          }
        ]
      }
    }
  });

  // Register the tasks.
  grunt.registerTask('default', ['uglify', 'sass', 'cssmin', 'postcss', 'watch']);
  grunt.registerTask('archive', ['compress']);
};
