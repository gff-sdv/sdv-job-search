<?php

/*
Plugin Name: SDV Job Search
Description: GFF SDV job search to find jobs (Provider: Yousty).
Version: 1.6
Author: GFF Integrative Kommunikation GmbH
Author URI: https://www.gff.ch
License: Proprietary
Text Domain: job-search
Domain Path: /locales
*/

declare( strict_types=1 );

use SDV_Job_Search\Job_Search_Cron;

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

/**
 * Plugin version.
 */
define( 'SDV_JOB_SEARCH_VERSION', '1.6' );

/**
 * Absolute filesystem path to the plugin root file.
 */
define( 'SDV_JOB_SEARCH_ABS_PLUGIN_FILE', __FILE__ );

/**
 * Absolute filesystem path to the plugin.
 */
define( 'SDV_JOB_SEARCH_ABS_PATH', dirname( __FILE__ ) );

/**
 * Root url of the plugin.
 */
define( 'SDV_JOB_SEARCH_ROOT_URL', plugin_dir_url( __FILE__ ) );

/**
 * Check dependencies.
 */
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
if ( is_plugin_active( 'sdv-store-locator/sdv-store-locator.php' ) ) {
	include_once( SDV_JOB_SEARCH_ABS_PATH . '/includes/bootstrap.php' );
} else {
	add_action( 'admin_notices', function () {
		$class   = 'notice notice-error';
		$message = __(
			'SDV Job Search needs the SDV Store Locator plugin to be installed and activated.',
			'job-search'
		);

		printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) );
	} );
}

/**
 * Deactivation Hook.
 */
register_deactivation_hook( __FILE__, function () {
	$timestamp = wp_next_scheduled( Job_Search_Cron::HOOK_NAME );
	if ( is_numeric( $timestamp ) ) {
		wp_unschedule_event( $timestamp, Job_Search_Cron::HOOK_NAME );
	}
} );
