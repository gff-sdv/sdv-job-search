=== SDV Job Search ===
Contributors: masterida
Donate link: https://www.gff.ch/
Requires at least: 5.0
Tested up to: 6.0
Requires PHP: 7.4
Stable tag: 1.6
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Provides the search for jobs.

== Description ==

The **SDV Job Search** plugin provides a search to find apprenticeships and trial
apprenticeships using a data provider api (currently: Yousty).

The plugin depends on the **SDV Store Locator** (see https://gitlab.com/gff-sdv/sdv-store-locator) plugin.

== Changelog ==

= 1.6 =
* Increase timeout for requests to the data provider.

= 1.1 =
* Initial version.
