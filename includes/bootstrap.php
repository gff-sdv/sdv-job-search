<?php

declare( strict_types=1 );

use SDV_Job_Search\Job_Search;

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

/**
 * Gets the current `Job_Search` instance. If there is currently no instance, this function would instantiate one.
 *
 * @return Job_Search The current instance.
 */
function sdv_job_search(): Job_Search {
	static $instance = null;

	if ( is_null( $instance ) ) {
		$instance = new Job_Search();
	}

	return $instance;
}

////////////////////////////// PHP CLASS AUTOLOAD //////////////////////////////

/**
 * The autoload function for `Job_Search` (namespace of job search) classes.
 *
 * Note that the namespaces for all `Job_Search` classes should never be prefixed by `\`.
 */
spl_autoload_register( function ( string $class_fqn ): void {
	static $base_path = SDV_JOB_SEARCH_ABS_PATH . '/includes/';

	// Get the first characters of the class fqn as they would be checked afterwards.
	$class_fqn_begin = substr( $class_fqn, 0, 15 );
	if ( 'SDV_Job_Search\\' !== $class_fqn_begin ) {
		return;
	}

	/** @noinspection DuplicatedCode */
	$p = strripos( $class_fqn, '\\' );
	if ( false === $p ) {
		$namespace  = '';
		$class_name = $class_fqn;
	} else {
		$namespace  = substr( $class_fqn, 0, $p );
		$class_name = substr( $class_fqn, $p + 1 );
	}

	$class_file = 'class-' . str_replace( '_', '-', strtolower( $class_name ) ) . '.php';
	$class_path = $base_path . (
		! empty( $namespace ) ?
			str_replace( [ '_', '\\' ], [ '-', '/' ], strtolower( $namespace ) ) . '/' :
			''
		) . $class_file;

	if ( is_file( $class_path ) ) {
		include_once( $class_path );
	} else {
		trigger_error( 'Class <b>' . $class_fqn . '</b> file not found.', E_USER_ERROR );
	}
} );

//////////////////////////// Job-Search ////////////////////////////

sdv_job_search();

////////////////////// Update Checker //////////////////////

add_action( 'plugins_loaded', function () {
	$updateChecker = Puc_v4_Factory::buildUpdateChecker(
		'https://gitlab.com/gff-sdv/sdv-job-search/',
		SDV_JOB_SEARCH_ABS_PLUGIN_FILE,
		'sdv-job-search'
	);

	/** @var Puc_v4p11_Vcs_GitLabApi $vcsApi */
	$vcsApi = $updateChecker->getVcsApi();
	$vcsApi->enableReleasePackages();
} );
