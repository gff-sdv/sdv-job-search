<?php

declare( strict_types=1 );

namespace SDV_Job_Search;

class Shortcode {

	protected Job_Search_Data $data;

	/**
	 * Constructor.
	 */
	public function __construct( Job_Search_Data $data ) {
		$this->data = $data;

		add_action( 'init', [ $this, 'init' ] );
	}

	/**
	 * Hook `init` handler.
	 *
	 * Initializes the shortcodes for the job-search.
	 */
	public function init(): void {
		// Register the shortcode for the store-locator.
		add_shortcode( 'job-search', [ $this, 'shortcode' ] );

		///
		// Register scripts and styles used in the frontend for the job-search shortcode.
		///
		wp_register_script(
			'job-search-view',
			SDV_JOB_SEARCH_ROOT_URL . 'scripts/view.js',
			[ 'store-locator-view' ],
			SDV_JOB_SEARCH_VERSION
		);
		wp_register_style(
			'job-search-view',
			SDV_JOB_SEARCH_ROOT_URL . 'styles/view.css',
			[ 'store-locator-autocomplete' ],
			SDV_JOB_SEARCH_VERSION
		);
	}

	/**
	 * @param array<string, mixed>|null $atts
	 * @param string|null               $content
	 * @param string                    $tag
	 *
	 * @return string
	 */
	public function shortcode( $atts = [], $content = null, $tag = '' ): string {
		wp_enqueue_script( 'google-maps' );
		wp_enqueue_script( 'job-search-view' );
		wp_enqueue_style( 'job-search-view' );

		$mode = $this->data->invalidate_search_mode(
			is_array( $atts ) && array_key_exists( 'mode', $atts ) ? $atts['mode'] : ''
		);

		// Build the location url (used for the API requests to find locations by
		// zip code of location names).
		$location_url = add_query_arg( [
			'_nonce' => sdv_store_locator()->get_location_api()->create_nonce(),
			'action' => sdv_store_locator()->get_location_api()->get_action(),
		], admin_url( 'admin-ajax.php' ) );

		// Build the search url (used for the API requests to find jobs).
		$search_url = add_query_arg( [
			'_nonce' => sdv_job_search()->get_search_api()->create_nonce(),
			'action' => sdv_job_search()->get_search_api()->get_action(),
			'mode'   => $mode,
		], admin_url( 'admin-ajax.php' ) );

		// Build a unique map id.
		$map_uid = wp_unique_id( 'map-' );

		return '<div class="job-search" data-search-url="' . esc_attr( $search_url ) . '" data-job-search-mode="' . esc_attr( $mode ) . '">
<div class="autocomplete-wrapper">
	<input type="search" spellcheck=false ' .
		       'placeholder="' . esc_attr__( 'Postal Code / Location', 'job-search' ) . '" ' .
		       'autocomplete="off" autocapitalize="off" maxlength="255" ' .
		       'data-autocomplete-url="' . esc_attr( $location_url ) . '" ' .
		       'data-autocomplete-no-results="Nichts gefunden.">
</div>
<div id="' . esc_attr( $map_uid ) . '" class="job-search-map" data-map></div>
<div class="job-search-results" data-results></div>
</div>
';
	}
}
