<?php

declare( strict_types=1 );

namespace SDV_Job_Search;

use function add_action;
use function time;
use function wp_next_scheduled;
use function wp_schedule_event;

class Job_Search_Cron {

	const HOOK_NAME = 'job_search_cron_hook';

	protected Job_Search_Data $data;

	public function __construct( Job_Search_Data $data ) {
		$this->data = $data;

		add_action( self::HOOK_NAME, [ $this, 'run' ] );

		if ( ! wp_next_scheduled( self::HOOK_NAME ) ) {
			wp_schedule_event( time(), 'hourly', self::HOOK_NAME );
		}
	}

	/**
	 * @return void
	 */
	public function run(): void {
		$this->data->refresh();
	}
}
