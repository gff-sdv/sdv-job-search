<?php

declare( strict_types=1 );

namespace SDV_Job_Search;

use RuntimeException;
use WP_Error;

class Job_Search_Data {

	const OPTION_NAME = 'job_search_data';

	const SEARCH_MODE_DEFAULT = 'default';

	const SEARCH_MODE_TRIAL_APPRENTICESHIP = 'trial_apprenticeship';

	/**
	 * @var string[]
	 */
	protected array $search_modes;

	protected string $url;

	public function __construct( string $url ) {
		$this->url          = $url;
		$this->search_modes = [ self::SEARCH_MODE_DEFAULT, self::SEARCH_MODE_TRIAL_APPRENTICESHIP ];
	}

	/**
	 * Get the search modes.
	 *
	 * @return string[]
	 */
	public function get_search_modes(): array {
		return $this->search_modes;
	}

	public function invalidate_search_mode( string $mode ): string {
		if ( in_array( $mode, $this->search_modes ) ) {
			return $mode;
		}

		if ( empty( $this->search_modes ) ) {
			throw new RuntimeException( 'No modes defined.' );
		}

		return $this->search_modes[0];
	}

	protected function send_email_to_admin( string $subject, string $message, array $attachments = [] ): void {
		$to      = get_option( 'admin_email' );
		$headers = array( 'Content-Type: text/html; charset=UTF-8' );

		wp_mail( $to, $subject, $message, $headers, $attachments );
	}

	public function refresh(): void {
		$url = $this->url;

		$result = wp_remote_get( $url, [ 'timeout' => 15 ] );
		if ( ! is_array( $result ) || ! array_key_exists( 'body', $result ) ) {
			// Skip this refresh, but inform the admin that something failed.
			$details = '';
			if ( $result instanceof WP_Error ) {
				$details = sprintf( '%s [%s]', $result->get_error_message(), $result->get_error_code() );
			}

			$this->send_email_to_admin(
				get_bloginfo( 'name' ) . ': Job Search Data Failure',
				'The url ' . esc_html( $url ) . ' could not be fetched. ' .
				esc_html( $details )
			);

			return;
		}

		$json = json_decode( $result['body'], true );
		if ( ! is_array( $json ) ) {
			// Skip this refresh, but inform the admin that the json was invalid.
			$attachment_body = wp_tempnam( 'body.txt' );
			file_put_contents( $attachment_body, $result['body'] );

			$this->send_email_to_admin(
				get_bloginfo( 'name' ) . ': Job Search Data Failure',
				'The fetched body from ' . esc_html( $url ) . ' could not be json decoded as array. ' .
				esc_html( json_last_error_msg() ),
				[
					$attachment_body
				]
			);

			@unlink( $attachment_body );

			return;
		}

		update_option( self::OPTION_NAME, $json );
	}

	public function get_data(): array {
		return get_option( self::OPTION_NAME, [] );
	}

	public function search( float $lat, float $lng, string $mode ): array {
		$data = $this->get_data();

		if ( ! array_key_exists( 'results', $data ) ) {
			return [];
		}

		$mode    = $this->invalidate_search_mode( $mode );
		$key     = 'distance';
		$results = $data['results'];
		for ( $i = count( $results ) - 1; $i >= 0; $i -- ) {
			$results[ $i ][ $key ] = sqrt(
				pow( $lat - $results[ $i ]['lat'], 2 ) +
				pow( $lng - $results[ $i ]['lng'], 2 )
			);
		}

		usort( $results, function ( $a, $b ) use ( $key ) {
			if ( $a[ $key ] < $b[ $key ] ) {
				return - 1;
			}
			if ( $a[ $key ] > $b[ $key ] ) {
				return 1;
			}

			return 0;
		} );

		return array_values(
			array_filter( $results, function ( $result ) use ( $mode ) {
				if ( $result['distance'] > 0.35 ) {
					return false;
				}

				if ( $mode === self::SEARCH_MODE_TRIAL_APPRENTICESHIP ) {
					return $result['trial_apprenticeship'];
				}

				if ( $mode === self::SEARCH_MODE_DEFAULT ) {
					return (
						$result['year_1'][1] || $result['year_2'][1] || $result['year_3'][1]
					);
				}

				return false;
			} )
		);
	}
}
