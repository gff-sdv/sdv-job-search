<?php

declare( strict_types=1 );

namespace SDV_Job_Search;

class Job_Search_Options {

	const SLUG = 'job_search_options';

	const OPTION_PAGE = 'job-search-page';

	const OPTION_GROUP = 'job-search';

	const OPTION_NAME = 'job-search';

	const SECTION_ID = 'job-search-section';

	const SETTING_DATA_PROVIDER_URL_ID = 'data-provider-url';

	public function __construct() {
		add_action( 'admin_menu', [ $this, 'admin_menu' ] );
		add_action( 'admin_init', [ $this, 'admin_init' ] );
	}

	/**
	 * Hook `admin_init` handler.
	 */
	public function admin_init(): void {
		register_setting( self::OPTION_GROUP, self::OPTION_NAME );

		add_settings_section(
			self::SECTION_ID,
			__( 'Data Provider', 'job-search' ),
			[ $this, 'section_callback' ],
			self::OPTION_PAGE
		);

		add_settings_field(
			self::SETTING_DATA_PROVIDER_URL_ID,
			__( 'Data Provider URL', 'job-search' ),
			[ $this, 'data_provider_url_render' ],
			self::OPTION_PAGE,
			self::SECTION_ID,
			[
				'label_for' => self::SETTING_DATA_PROVIDER_URL_ID,
			]
		);
	}

	/**
	 * Gets all options.
	 *
	 * @return array<string, mixed>
	 */
	public function get_options(): array {
		$options = get_option( self::OPTION_NAME );

		return array_merge(
			[
				self::SETTING_DATA_PROVIDER_URL_ID => '',
			],
			is_array( $options ) ? $options : []
		);
	}

	/**
	 * Gets the data provider url.
	 *
	 * @return string
	 */
	public function get_option_data_provider_url(): string {
		$options = $this->get_options();

		return $options[ self::SETTING_DATA_PROVIDER_URL_ID ];
	}

	/**
	 * Data Provider URL input control renderer.
	 */
	public function data_provider_url_render(): void {
		$options = $this->get_options();

		echo '<input id="' . self::SETTING_DATA_PROVIDER_URL_ID . '" type="text" class="regular-text" name="' . self::OPTION_NAME . '[' . self::SETTING_DATA_PROVIDER_URL_ID . ']" value="' . esc_attr( $options[ self::SETTING_DATA_PROVIDER_URL_ID ] ) . '">';
	}

	/**
	 * Section callback.
	 */
	public function section_callback(): void {
	}

	/**
	 * Hook `admin_menu` handler.
	 */
	public function admin_menu(): void {
		add_options_page(
			__( 'Job Search', 'job-search' ),
			__( 'Job Search', 'job-search' ),
			'manage_options',
			self::SLUG,
			[ $this, 'settings_page' ]
		);
	}

	/**
	 * Settings page display callback.
	 */
	public function settings_page(): void {
		echo '<div class="wrap"><h1>' .
		     __( 'Settings', 'default' ) . ' › ' .
		     __( 'Job Search', 'job-search' ) .
		     '</h1>';

		/** @noinspection HtmlUnknownTarget */
		echo '<form action="options.php" method="post">';

		settings_fields( self::OPTION_GROUP );
		do_settings_sections( self::OPTION_PAGE );
		submit_button();

		echo '</form>';
	}
}