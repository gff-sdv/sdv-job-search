<?php

declare( strict_types=1 );

namespace SDV_Job_Search;

class Job_Search {
	protected Job_Search_Data $data;

	protected Search_API $search_api;

	protected Shortcode $shortcode;

	protected Job_Search_Cron $cron;

	protected Job_Search_Options $options;

	public function __construct() {
		$this->options    = new Job_Search_Options();
		$this->data       = new Job_Search_Data(
			$this->options->get_option_data_provider_url()
		);
		$this->search_api = new Search_API( $this->data );
		$this->cron       = new Job_Search_Cron( $this->data );
		$this->shortcode  = new Shortcode( $this->data );

		// Set the `plugins_loaded` action handler (note that this hook gets called before the init hook).
		add_action( 'plugins_loaded', [ $this, 'plugins_loaded' ] );
	}

	/**
	 * @return Search_API
	 */
	public function get_search_api(): Search_API {
		return $this->search_api;
	}

	/**
	 * Hook `plugins_loaded` handler.
	 */
	public function plugins_loaded(): void {
		load_plugin_textdomain( 'job-search', false, 'sdv-job-search/locales' );
	}
}
