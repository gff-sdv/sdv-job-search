<?php

declare( strict_types=1 );

namespace SDV_Job_Search;

class Search_API {
	protected const ACTION_NAME = 'job_search_search';

	protected Job_Search_Data $data;

	public function __construct( Job_Search_Data $data ) {
		$this->data = $data;

		add_action( 'wp_ajax_' . self::ACTION_NAME, [ $this, 'ajax_callback' ] );
		add_action( 'wp_ajax_nopriv_' . self::ACTION_NAME, [ $this, 'ajax_callback' ] );
	}

	public function ajax_callback() {
		check_ajax_referer( self::ACTION_NAME, '_nonce' );

		$lat  = filter_input( INPUT_GET, 'lat' );
		$lng  = filter_input( INPUT_GET, 'lng' );
		$mode = filter_input( INPUT_GET, 'mode' );

		$results = $this->data->search( floatval( $lat ), floatval( $lng ), (string) $mode );

		wp_send_json( [ 'lat' => $lat, 'lng' => $lng, 'results' => $results ] );
	}

	/**
	 * Creates a nonce for this API action.
	 *
	 * @return string
	 */
	public function create_nonce(): string {
		return wp_create_nonce( self::ACTION_NAME );
	}

	public function get_action(): string {
		return self::ACTION_NAME;
	}
}
