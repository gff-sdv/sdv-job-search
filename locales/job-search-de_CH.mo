��          \      �       �      �      �   3   �   
        (     ?  P   N  �  �     '     6  L   O     �     �     �  ?   �                                       Data Provider Data Provider URL GFF SDV job search to find jobs (Provider: Yousty). Job Search Postal Code / Location SDV Job Search SDV Job Search needs the SDV Store Locator plugin to be installed and activated. Project-Id-Version: Job Search 1.0
Report-Msgid-Bugs-To: adrian.suter@gff.ch
PO-Revision-Date: 2022-06-20 16:28+0200
Last-Translator: Adrian Suter <adrian.suter@gff.ch>
Language-Team: GFF
Language: de_CH
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.1
X-Poedit-SourceCharset: UTF-8
 Datenlieferant URL des Datenlieferanten GFF SDV Jobsuche stellt eine Jobsuche zur Verfügung (Datenliferant: Yousty) Jobsuche Gib hier den Ort ein SDV Jobsuche Das SDV Jobsuche Plugin benötigt das SDV Drogeriesuche Plugin. 